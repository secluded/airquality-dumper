var AWS = require("aws-sdk")
var mqtt = require('mqtt')
var config = require('./config.json')

AWS.config.update({
  region: "ap-southeast-2",
  credentials: new AWS.SharedIniFileCredentials({
      profile: 'stratager-root'
  })
})

var docClient = new AWS.DynamoDB.DocumentClient()
var client  = mqtt.connect(config.mqtt)

client.on('connect', function () {
  console.log('connected')
  client.subscribe('/aq')
})

client.on('message', function (topic, message) {

  console.log('Received MQTT Message ' + message.toString())
  if (level = parseInt(message, 10)) {

    var table = "stations"
    var id = "GCTS"
    var timestamp = Date.now()

    var params = {
        TableName:table,
        Item:{
            "id": id,
            "timestamp": timestamp,
            "level": level
        }
    }

    console.log("Pushing data to Dynamo")
    docClient.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2))
        } else {
            console.log("Added item:", JSON.stringify(data, null, 2))
        }
    })
  }
})
